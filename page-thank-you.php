<?php
/**
 * Template Name: Thank You Page Template 
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/fonts/gotham-font.css' type='text/css' media='all' />
<style>

	body * {
		font-family: 'Gotham', sans-serif !important;
	}
	#wb-thank-you-page {
		padding:0;
		margin:0;
		height:calc(100vh - 180px);
	}
	#wb-thank-you-page .wb-thank-you-page-header {
		background: #005C98;
	}
	#wb-thank-you-page .wb-thank-you-page-header .wb-thank-you-page-header-wrap {
		display:flex;
		align-items: center;
  		justify-content: center;
		padding:80px 40px 160px;
	}
	
	#wb-thank-you-page .wb-thank-you-page-header .wb-logo .logo {
		border-right:1px solid #fff;
		padding-right:40px;
	}
	#wb-thank-you-page .wb-thank-you-page-header .wb-logo img {
		display:block;
		height:auto;
		max-width:138px;
	}
	#wb-thank-you-page .wb-thank-you-page-header .wb-top-content {
		text-align:center;
		padding-left:40px;
	}
	#wb-thank-you-page .wb-thank-you-page-header h3,
	#wb-thank-you-page .wb-thank-you-page-header h2 {
		margin:0;
		padding:0;
		color:#fff;
		letter-spacing: 0.05em;
	}
	#wb-thank-you-page .wb-thank-you-page-header h3:before,
	#wb-thank-you-page .wb-thank-you-page-header h2:before {
		display:none;
	}
	#wb-thank-you-page .wb-thank-you-page-header h2 {
		font-size:26px;
		letter-spacing: 0px
		font-weight:500;
	}
	#wb-thank-you-page .wb-thank-you-page-header h3 {
		font-size:20px;
		font-weight: 200;
	}

	#wb-thank-you-page .wb-main-content {
		transform: translateY(-130px);
		text-align: center;
		padding:0 40px 40px;
	}
	#wb-thank-you-page .wb-main-content .wb-featured-image {
		max-width:716px;
		margin:0 auto;
	}
	#wb-thank-you-page .wb-main-content .wb-content {
		max-width:573px;
		margin:0 auto;
		font-size:18px;
	}

	@media (max-width:767px) {
		#wb-thank-you-page .wb-thank-you-page-header .wb-thank-you-page-header-wrap {
			display:block;
		}
		#wb-thank-you-page .wb-thank-you-page-header .wb-logo .logo {
			padding:0;
			border:none;
			text-align:center;
			margin-bottom:20px;
		}
		#wb-thank-you-page .wb-thank-you-page-header .wb-logo .logo img {
			margin:0 auto;
		}
		#wb-thank-you-page .wb-thank-you-page-header .wb-top-content {
			padding:0;
		}
		#wb-thank-you-page .wb-thank-you-page-header .wb-top-content h2{
			margin-bottom:20px;
		}
	}


</style>

<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<div id="wb-thank-you-page" >
	
	<div class="wb-thank-you-page-header">
		<div class="wb-thank-you-page-header-wrap">
			<div class="wb-logo">
				<div class="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/images/CKN_Logo_WHITE-RGB@2x.png">
				</div>
			</div>
			<div class="wb-top-content">
				<div class="wb-content">
					<h2>Sorry, we're down for scheduled maintenance</h2>
					<h3>July 4th, 2020 - 7:00p CT through July 5th, 2020 - 7:00a CT</h3>
				</div>
			</div>
		</div>
	</div>

	<div class="wb-main-content">
		<div class="wb-featured-image">
			<img src="<?php echo get_template_directory_uri(); ?>/images/CKN-multi-device@2x.png">
		</div>
		<div class="wb-content">
			
			<p>We thank you for your patience as we perform scheduled updates and maintenance. We'll be back soon!</p>
		</div>
	</div>

</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>